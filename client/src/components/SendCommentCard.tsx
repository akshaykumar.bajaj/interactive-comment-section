import React, { useEffect, useState } from 'react'

function SendCommentCard() {
    const [avatarPath, setAvatarPath] = useState<string | null>('');

    useEffect(()=>{
        setAvatarPath(localStorage.getItem("currentUserAvatarPath"))
    },[])

    return (
        <div className='SendCommentCard CommentCard bg-very-light-gray rounded-lg m-5 p-5 flex items-start justify-between flex-wrap'>
            <textarea className='rounded-lg flex-grow m-2 p-2 bg-very-light-gray border-2 border-light-gray place-holder-light-gray focus:border-moderate-blue focus-visible:border-moderate-blue' name="newComment" id="newComment" cols={20} rows={4} placeholder='Add a comment...'></textarea>
            <img src={process.env.PUBLIC_URL + avatarPath} alt="current-user-avatar" className='lg:order-first object-contain h-12 m-2'/>
            <button type="submit" className='bg-moderate-blue text-white rounded-md m-2 py-3 px-6 font-medium opacity-50 hover:opacity-100'>SEND</button>
        </div>
    )
}

export default SendCommentCard