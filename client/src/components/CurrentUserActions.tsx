import React from 'react'
import deleteIcon from "./images/icon-delete.svg"
import editIcon from "./images/icon-edit.svg"

function CurrentUserActions() {
    return (
        <div className="currentUserActions flex space-x-2">
            <button className='flex justify-between opacity-50 hover:opacity-100'>
                <img src={deleteIcon} alt="delete-icon" className='object-contain m-1' /> <span className='text-soft-red font-medium'>Delete</span>
            </button>
            <button className='flex justify-between opacity-50 hover:opacity-100'>
                <img src={editIcon} alt="edit-icon" className='object-contain m-1' /> <span className='text-moderate-blue font-medium'>Edit</span>
            </button>
        </div>
    )
}

export default CurrentUserActions