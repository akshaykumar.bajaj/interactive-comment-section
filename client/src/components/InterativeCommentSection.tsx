import React, { useEffect } from 'react'
import CommentCard from './CommentCard'
import SendCommentCard from './SendCommentCard'
import data from "../data.json"

function InterativeCommentSection() {

  useEffect(() => {
    localStorage.setItem("currentUserName", data.currentUser.username)
    localStorage.setItem("currentUserAvatarPath", data.currentUser.image.webp)
  }, [])

  return (
    <div className='InterativeCommentSection container bg-light-gray lg:w-1/2'>
      {
        data.comments.map((commentDeatils) => {
          return <>
            <CommentCard commentDeatils={commentDeatils} isReply={false} key={commentDeatils.id} />
            {
              commentDeatils.replies.length !== 0 &&
                <div className="replies lg:ml-10 lg:border-l border-light-grayish-blue">
                  {
                    commentDeatils.replies.map((reply) => {
                      return <>
                        <CommentCard commentDeatils={reply} isReply={true} key={commentDeatils.id} />
                      </>
                    })
                  }
                </div>
            }
          </>
        })
      }
      <SendCommentCard />
    </div>
  )
}

export default InterativeCommentSection