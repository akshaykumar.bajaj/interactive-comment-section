import React, { useEffect, useState } from 'react'
import plusIcon from "./images/icon-plus.svg"
import minusIcon from "./images/icon-minus.svg"
import replyIcon from "./images/icon-reply.svg"
import CurrentUserActions from './CurrentUserActions';


function CommentCard(props: { commentDeatils: any, isReply: boolean }) {
  const [isCurrentUserName, setIsCurrentUserName] = useState<boolean>(false);
  const [score, setScore] = useState<number>(props.commentDeatils.score)

  useEffect(() => {
    let currentUserName = localStorage.getItem("currentUserName")
    setIsCurrentUserName(currentUserName === props.commentDeatils.user.username)
  }, [])

  return (
    <div className='CommentCard bg-very-light-gray rounded-lg m-5 p-5 relative flex flex-wrap lg:flex-nowrap'>
      <div className="comment">
        <div className='commentData h-8 flex space-x-2 lg:space-x-4'>
          <img src={process.env.PUBLIC_URL + props.commentDeatils.user.image.webp} alt="user-avatar" className='object-contain rounded-full' />
          <span className='text-dark-blue font-bold'>{props.commentDeatils.user.username}</span>
          {
            //Conditoinal rendering for current user
            isCurrentUserName && <span className='bg-moderate-blue text-white h-fit p-1 text-xs font-medium'>you</span>
          }
          <span className='text-grayish-blue font-normal'>{
            props.commentDeatils.createdAt
          }</span>
        </div>
        <p className="commentContent text-grayish-blue my-2 lg:m-2">
          {props.isReply && <span className='text-moderate-blue font-bold'>@{props.commentDeatils.replyingTo} </span>}
          {props.commentDeatils.content}
        </p>
      </div>
      <div className="votes lg:order-first bg-light-gray rounded-lg p-2 flex justify-between lg:flex-col lg:m-4 lg:px-1">
        <button id="upVoteBtn" onClick={() => setScore(score + 1)}>
          <img src={plusIcon} alt="plus-icon" className='mx-2 object-contain' />
        </button>
        <p className='bg-light-gary text-moderate-blue font-medium mx-2'>{
          score
        }</p>
        <button id="downVoteBtn" onClick={() => setScore(score - 1)}>
          <img src={minusIcon} alt="minus-icon" className='mx-2 object-contain' />
        </button>
      </div>
      <div className="actions p-1 absolute bottom-6 right-2 lg:top-4">
        {isCurrentUserName ? <CurrentUserActions /> : 
        <button className='flex justify-between opacity-50 hover:opacity-100'>
          <img src={replyIcon} alt="reply-icon" className='object-contain m-1' /> <span className='text-moderate-blue font-medium'>Reply</span>
        </button>}
      </div>
    </div>
  )
}

export default CommentCard