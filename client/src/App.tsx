import React from 'react';
import './App.css';
import InterativeCommentSection from './components/InterativeCommentSection';

function App() {
  return (
    <div className="App bg-light-gray flex justify-center">
      <InterativeCommentSection />
    </div>
  );
}

export default App;
